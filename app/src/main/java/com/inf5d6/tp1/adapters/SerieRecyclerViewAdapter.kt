package com.inf5d6.tp1.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.os.bundleOf
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.inf5d6.tp1.R
import com.inf5d6.tp1.models.TvShow
import com.squareup.picasso.Picasso

class SerieRecyclerViewAdapter(private val datasetSerie: MutableList<TvShow>) :
    RecyclerView.Adapter<SerieRecyclerViewAdapter.SerieViewHolder>(){

    class SerieViewHolder(val view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SerieViewHolder {

        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_serie_item, parent, false) as View
        return SerieViewHolder(view)
    }

    override fun onBindViewHolder(holderSerie: SerieViewHolder, position: Int) {

        val imgSerie = holderSerie.view.findViewById<ImageView>(R.id.imgSerie)
        Picasso.get().load(this.datasetSerie[position].Image).into(imgSerie)

        holderSerie.view.setOnClickListener {
            val param = bundleOf(Pair("TvShowId", this.datasetSerie[position].TvShowId))
            holderSerie.view.findNavController()
                .navigate(R.id.navigation_detail, param)
        }
    }

    override fun getItemCount(): Int {
        return this.datasetSerie.size
    }


}