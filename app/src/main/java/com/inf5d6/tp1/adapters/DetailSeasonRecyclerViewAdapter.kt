package com.inf5d6.tp1.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.inf5d6.tp1.R
import com.inf5d6.tp1.models.Season
import com.squareup.picasso.Picasso

class DetailSeasonRecyclerViewAdapter(private val seasonTvShow: MutableList<Season>) :
    RecyclerView.Adapter<DetailSeasonRecyclerViewAdapter.DetailSeasonViewHolder>() {

    class DetailSeasonViewHolder(val view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):
            DetailSeasonViewHolder {
        println("DEBUG: DetailSeasonRecyclerViewAdapter:onCreateViewHolder:seasonTvShow: $seasonTvShow")
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_detail_season_item, parent, false)
        return DetailSeasonViewHolder(view)
    }

    override fun onBindViewHolder(holderSeasonDetail: DetailSeasonViewHolder, position: Int) {

        val imageSeason = holderSeasonDetail.view.findViewById<ImageView>(R.id.ivSeasonImage)
        Picasso.get().load(this.seasonTvShow[position].Image).into(imageSeason)

        holderSeasonDetail.view.findViewById<TextView>(R.id.tvSeasonNumber).text =
            this.seasonTvShow[position].Number.toString()
        println("DEBUG: DetailSeasonRecyclerViewAdapter:onBindViewHolder:this.seasonTvShow[position].Number.toString():" + this.seasonTvShow[position].Number.toString())

        holderSeasonDetail.view.findViewById<TextView>(R.id.tvSeasonEpisodeCount).text =
            this.seasonTvShow[position].EpisodeCount.toString()

        println("DEBUG: DetailSeasonRecyclerViewAdapter:onBindViewHolder: this.seasonTvShow[position].EpisodeCount.toString():" +  this.seasonTvShow[position].EpisodeCount.toString())


        println("DEBUG: DetailSeasonRecyclerViewAdapter:onBindViewHolder:this.seasonTvShow.size: " + this.seasonTvShow.size)

    }

    override fun getItemCount(): Int = this.seasonTvShow.size
}
