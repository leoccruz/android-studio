package com.inf5d6.tp1.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.inf5d6.tp1.R
import com.inf5d6.tp1.models.Role
import com.squareup.picasso.Picasso

class DetailCastRecyclerViewAdapter(private val castTvShow: MutableList<Role>) :
    RecyclerView.Adapter<DetailCastRecyclerViewAdapter.DetailCastViewHolder>() {

    class DetailCastViewHolder(val view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):
            DetailCastViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_detail_cast_item, parent, false)
        return DetailCastViewHolder(view)
    }

    override fun onBindViewHolder(holderCastDetail: DetailCastViewHolder, position: Int) {

        val photoArtiste = holderCastDetail.view.findViewById<ImageView>(R.id.ivCharacterImage)
        Picasso.get().load(this.castTvShow[position].Image).into(photoArtiste)

        holderCastDetail.view.findViewById<TextView>(R.id.tvCharacterName).text =
            this.castTvShow[position].Character

        holderCastDetail.view.findViewById<TextView>(R.id.tvActName).text =
            this.castTvShow[position].Name

    }

    override fun getItemCount(): Int = this.castTvShow.size

}