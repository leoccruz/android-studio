package com.inf5d6.tp1.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.inf5d6.tp1.R
import com.inf5d6.tp1.adapters.SerieRecyclerViewAdapter
import com.inf5d6.tp1.models.TvShow

class HomeFragment : Fragment() {
    // view model et recycler view pour la page home
    private lateinit var homeViewModel: HomeViewModel
    private val datasetSerie: MutableList<TvShow> = mutableListOf()
    private lateinit var rvListeSerie : RecyclerView
    private val serieRecyclerViewAdapter = SerieRecyclerViewAdapter(datasetSerie)

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        this.rvListeSerie = view.findViewById(R.id.rvSerie)
        this.rvListeSerie.layoutManager = GridLayoutManager(this.activity, 2)
        this.rvListeSerie.adapter = serieRecyclerViewAdapter

        // recuperer les donnees
        this.homeViewModel =
            ViewModelProvider(this).get(HomeViewModel::class.java)

        // observe les donnees
        this.homeViewModel.series.observe(viewLifecycleOwner, {
            this.datasetSerie.clear()
            this.datasetSerie.addAll(it)
            this.serieRecyclerViewAdapter.notifyDataSetChanged()
            // Debug
            println("DEBUG : HomeFragment:onViewCreated:datasetSerie: $datasetSerie")
        })// fin observe
    } //fin onViewCreated
}