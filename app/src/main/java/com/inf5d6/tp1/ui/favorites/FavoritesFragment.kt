package com.inf5d6.tp1.ui.favorites

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.inf5d6.tp1.R
import com.inf5d6.tp1.adapters.SerieRecyclerViewAdapter
import com.inf5d6.tp1.models.TvShow
import com.inf5d6.tp1.ui.detail.DetailViewModel
import com.inf5d6.tp1.ui.detail.DetailViewModelFactory
import com.squareup.picasso.Picasso

class FavoritesFragment : Fragment() {

    private val dataFavoris : MutableList<TvShow> = mutableListOf()
    private lateinit var rvFavoris : RecyclerView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_favorites, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val sharedPref = requireActivity().getPreferences(Context.MODE_PRIVATE)
        val brToken = sharedPref.getString("bearerToken", "").toString()
        rvFavoris = view.findViewById<RecyclerView>(R.id.rvFavoris)
        rvFavoris.layoutManager = GridLayoutManager(this.activity,2 )
        rvFavoris.adapter = SerieRecyclerViewAdapter(this.dataFavoris)

        val favorisViewModelFactory = FavoritesViewModelFactory(this.requireActivity().application, brToken)
        val favorisViewModel = ViewModelProvider(this, favorisViewModelFactory).get(FavoritesViewModel::class.java)

        favorisViewModel.listeFavoris.observe(viewLifecycleOwner, {
            this.dataFavoris.clear()
            this.dataFavoris.addAll(it)
            this.rvFavoris.adapter!!.notifyDataSetChanged()

        })
    }
}
