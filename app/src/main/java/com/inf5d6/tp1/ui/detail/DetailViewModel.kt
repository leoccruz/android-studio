package com.inf5d6.tp1.ui.detail

import android.app.Application
import android.content.Context
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.inf5d6.tp1.models.DetailsTvShow
import com.inf5d6.tp1.repositories.DetailRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class DetailViewModel (application: Application, tvShowId: Int): AndroidViewModel(application) {
    val detail: MutableLiveData<DetailsTvShow> = MutableLiveData()
    val estFavoris: MutableLiveData<Boolean> = MutableLiveData()

    init {
        viewModelScope.launch(Dispatchers.IO) {
            val detailRepository = DetailRepository(getApplication())
            detailRepository.getDetail(tvShowId, detail)
            println("DEBUG: DetailViewModel:init:tvShowId: $tvShowId")
            println("DEBUG: DetailViewModel:init:detailRepository: $detailRepository")
            println("DEBUG: DetailViewModel:init:detail: $detail")
        }
    }
    fun estFavorite(tvShowId: Int, token:String) {
        //appeller un repo qui modifie estFavoris
        val detailRepository = DetailRepository(getApplication())
        detailRepository.getFavStatus(tvShowId, token, estFavoris )
    }

    fun suppFav(tvShowId: Int, token:String) {
        viewModelScope.launch(Dispatchers.IO) {
            val supprimerfav = DetailRepository(getApplication())
            supprimerfav.delete(tvShowId, token, estFavoris)
        }

    }

    fun ajouteFav(tvShowId: Int, token:String) {
        viewModelScope.launch(Dispatchers.IO) {
            val ajtFav = DetailRepository(getApplication())
            ajtFav.add(tvShowId, token, estFavoris)
        }
    }
}
