package com.inf5d6.tp1.ui.favorites

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.inf5d6.tp1.models.TvShow
import com.inf5d6.tp1.repositories.AuthenticationRequestRepository
import com.inf5d6.tp1.repositories.AuthenticationStringRequestRepository
import com.inf5d6.tp1.repositories.DetailRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class FavoritesViewModel (application: Application, brToken: String) : AndroidViewModel(application) {
    public val listeFavoris: MutableLiveData<MutableList<TvShow>> = MutableLiveData(mutableListOf())
    init {
        viewModelScope.launch(Dispatchers.IO) {
            val favorisRepository = AuthenticationStringRequestRepository(getApplication())
            favorisRepository.getFav(listeFavoris, brToken)
        }
    }
}