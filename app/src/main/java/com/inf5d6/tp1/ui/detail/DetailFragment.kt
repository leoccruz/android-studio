package com.inf5d6.tp1.ui.detail

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.inf5d6.tp1.R
import com.inf5d6.tp1.adapters.DetailCastRecyclerViewAdapter
import com.inf5d6.tp1.adapters.DetailSeasonRecyclerViewAdapter
import com.inf5d6.tp1.models.Role
import com.inf5d6.tp1.models.Season
import com.squareup.picasso.Picasso

class DetailFragment : Fragment() {

    // Recycler view pour les acteurs et actrices
    private lateinit var rvDetailCastTvShow : RecyclerView
    private val castTvShow: MutableList<Role> = mutableListOf() // liste des artistes
    private var detailCastRecyclerViewAdapter = DetailCastRecyclerViewAdapter(castTvShow)

    // Recycler view pour les saisons
    private lateinit var rvDetailSeasonTvShow : RecyclerView
    private val seasonTvShow: MutableList<Season> = mutableListOf() // liste des saisons
    private val detailSeasonRecyclerViewAdapter = DetailSeasonRecyclerViewAdapter(seasonTvShow)

    //Appele le fragment layout
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        // Pour afficher les listes dans l'adapter:

        rvDetailCastTvShow = view.findViewById(R.id.rvRoles)
        rvDetailCastTvShow.layoutManager = LinearLayoutManager(this.activity,LinearLayoutManager.HORIZONTAL, false)
        rvDetailCastTvShow.adapter = detailCastRecyclerViewAdapter

        rvDetailSeasonTvShow = view.findViewById(R.id.rvSeasons)
        rvDetailSeasonTvShow.layoutManager = LinearLayoutManager(this.activity,LinearLayoutManager.HORIZONTAL, false)
        rvDetailSeasonTvShow.adapter = detailSeasonRecyclerViewAdapter

        // Pour partager recuperer le token
        val sharedPref = requireActivity().getPreferences(Context.MODE_PRIVATE)
        val token = sharedPref.getString("bearerToken", "").toString()
        println("DEBUG : DETAILFragment:onViewCreated:bearerToken: $token")

        // View model factory pour les données selon l'ID de la serie
        val tvShowId = this.requireArguments().getInt("TvShowId", 1)
        val detailViewModelFactory = DetailViewModelFactory(this.requireActivity().application, tvShowId)
        val detailViewModel = ViewModelProvider(this, detailViewModelFactory).get(DetailViewModel::class.java)

        // Controles des components du layout
        val posterShow = view.findViewById<ImageView>(R.id.ivPoster)
        val titleShow = view.findViewById<TextView>(R.id.tvTitle)
        val yearShow = view.findViewById<TextView>(R.id.tvYear)
        val countEpisodesShow = view.findViewById<TextView>(R.id.tvTotalEpisodeCount)
        val plotShow = view.findViewById<TextView>(R.id.tvPlot)

        // Pour recuperer dans le model si une serie selections est favorite
        detailViewModel.estFavorite(tvShowId,token)

        // Boutton etoile pour supprimer un favoris
        view.findViewById<ImageButton>(R.id.btFavorisOn).setOnClickListener{
            detailViewModel.suppFav(tvShowId, token)
            view.findNavController().navigate(R.id.navigation_home)
        }

        // Boutton etoile pour ajouter un favoris
        view.findViewById<ImageButton>(R.id.btFavorisOff).setOnClickListener{
            detailViewModel.ajouteFav(tvShowId, token)
            view.findNavController().navigate(R.id.navigation_home)
        }

        // Pour inialiser la visibilite des boutons de favorites
        view.findViewById<ImageButton>(R.id.btFavorisOff).setVisibility(View.GONE)
        view.findViewById<ImageButton>(R.id.btFavorisOn).setVisibility(View.GONE)

        // Pour reagir a une modification du status de favorite
        detailViewModel.estFavoris.observe(viewLifecycleOwner, {

            if (it == true){
                view.findViewById<ImageButton>(R.id.btFavorisOn).setVisibility(View.VISIBLE)
            } else {
                view.findViewById<ImageButton>(R.id.btFavorisOff).setVisibility(View.VISIBLE)
            }

        })
        // Pour reagir a selection de la serie selon idTvshow
        detailViewModel.detail.observe(viewLifecycleOwner, {
            Picasso.get().load(it.Image).into(posterShow)
            titleShow.text = it.Title
            yearShow.text = it.Year.toString()
            countEpisodesShow.text = it.EpisodeCount.toString()
            plotShow.text = it.Plot

            // vider la liste des artistes
            this.castTvShow.clear()
            this.castTvShow.addAll(it.Roles)
            this.detailCastRecyclerViewAdapter.notifyDataSetChanged()

            // vide la liste des saisosn
            this.seasonTvShow.clear()
            this.seasonTvShow.addAll(it.Seasons)
            this.detailSeasonRecyclerViewAdapter.notifyDataSetChanged()

            println("DEBUG: DetailFragment:onViewCreated:seasonTvShow: $seasonTvShow")

        })
    }
}