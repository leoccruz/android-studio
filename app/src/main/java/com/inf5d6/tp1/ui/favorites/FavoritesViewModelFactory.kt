package com.inf5d6.tp1.ui.favorites

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.inf5d6.tp1.ui.detail.DetailViewModel

class FavoritesViewModelFactory (val application: Application, private val brToken: String) :
    ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(FavoritesViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return FavoritesViewModel(this.application, brToken) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}