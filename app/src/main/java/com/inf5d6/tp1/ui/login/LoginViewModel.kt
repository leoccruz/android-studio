package com.inf5d6.tp1.ui.login

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.inf5d6.tp1.repositories.AuthenticationRequestRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class LoginViewModel(application: Application) : AndroidViewModel(application) {
    var bearerToken: MutableLiveData<String> = MutableLiveData()

    fun doLogin(username: String, password: String){
        println("DEBUG : LoginViewModel:doLogin:username): $username")
        println("DEBUG : LoginViewModel:doLogin:password): $password")
        viewModelScope.launch (Dispatchers.IO){
            val authenticationRequestRepository = AuthenticationRequestRepository(getApplication())
            authenticationRequestRepository.setDataAuth(username, password)
            authenticationRequestRepository.getToken(bearerToken)
        }

    }
}
