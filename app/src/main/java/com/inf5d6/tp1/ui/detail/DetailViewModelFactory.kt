package com.inf5d6.tp1.ui.detail

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class DetailViewModelFactory (val application: Application, private val tvShowId: Int) :
    ViewModelProvider.Factory
{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        println("DEBUG: class details view factory:tvShowId: $tvShowId")
        if (modelClass.isAssignableFrom(DetailViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return DetailViewModel(this.application, this.tvShowId) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}