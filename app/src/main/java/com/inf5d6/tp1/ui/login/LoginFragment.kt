package com.inf5d6.tp1.ui.login

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import com.inf5d6.tp1.R

class LoginFragment : Fragment() {
    // view model pour le token
    private lateinit var loginViewModel: LoginViewModel
    var bearerToken = ""

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        this.loginViewModel = ViewModelProvider(this).get(LoginViewModel::class.java)

        println("DEBUG : LoginFragment:onViewCreated:bearerToken: $bearerToken")

        // pour recuperer les donnees des champs de login
        view.findViewById<Button>(R.id.btLogin).setOnClickListener{
            val username = view.findViewById<EditText>(R.id.etEmailAddress).text.toString()
            val password = view.findViewById<EditText>(R.id.etPassword).text.toString()
            println("DEBUG : LoginFragment:onViewCreated:setOnClickListener:username): $username")
            println("DEBUG : LoginFragment:onViewCreated:setOnClickListener:password): $password")
            this.loginViewModel.doLogin(username, password )
        }
        // pour reagir a une changement de token: il va le partager entre les fragments
        this.loginViewModel.bearerToken.observe(viewLifecycleOwner, {
            val sharedPref = requireActivity().getPreferences(Context.MODE_PRIVATE)
            sharedPref.edit().putString("bearerToken", it).apply()
            view.findNavController().navigate(R.id.navigation_favorites)
            // Debug
        })// fin observe
    }
}