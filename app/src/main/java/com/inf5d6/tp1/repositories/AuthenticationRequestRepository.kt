package com.inf5d6.tp1.repositories

import android.app.Application
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.Volley
import com.google.gson.Gson
import com.inf5d6.tp1.auth.AuthenticationRequest
import com.inf5d6.tp1.models.AuthenticationResponse

class AuthenticationRequestRepository (private val application: Application) {

    private var user = ""
    private var pwd = ""

    fun setDataAuth(username: String, password: String){
        println("DEBUG : AuthenticationRequestRepository:setDataAuth:username): $username")
        println("DEBUG : AuthenticationRequestRepository:setDataAuth:password): $password")
        this.user = username
        this.pwd = password
    }

    fun getToken(bearerToken:  MutableLiveData<String>) {
        val queue = Volley.newRequestQueue(application)
        @Suppress("RedundantSamConstructor")
        println("DEBUG : AuthenticationRequestRepository:getToken:user): " + this.user)
        println("DEBUG : AuthenticationRequestRepository:getToken:pwd): " + this.pwd)
        val r = AuthenticationRequest(
            method = Request.Method.POST,
            url = "https://tvshowapi.apis.redirectme.net/token",
            username = this.user,
            password = this.pwd,
            //username = "user2",
            //password = "pass2",
            Response.Listener {
                val auth = Gson().fromJson(it, AuthenticationResponse::class.java)
                Toast.makeText(application, "Login Réussi!", Toast.LENGTH_LONG).show()
                println("DEBUG : AuthenticationRequestRepository:getToken:auth.access_token: ${auth.access_token}")
                bearerToken.value = auth.access_token
            },
            Response.ErrorListener {
                Toast.makeText(application, "Login Invalide", Toast.LENGTH_LONG).show()
                println(it.message)
            }
        )
        //Ne pas oublier de placer la requête dans la file pour qu'elle soit exécutée
        queue.add(r)
    }

}