package com.inf5d6.tp1.repositories

import android.app.Application
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import com.android.volley.Request
import com.android.volley.Request.Method.GET
import com.android.volley.Response
import com.android.volley.toolbox.Volley
import com.google.gson.Gson
import com.inf5d6.tp1.auth.AuthenticatedStringRequest
import com.inf5d6.tp1.auth.AuthenticationRequest
import com.inf5d6.tp1.models.AuthenticationResponse
import com.inf5d6.tp1.models.TvShow
import java.lang.reflect.Method

class AuthenticationStringRequestRepository(private val application: Application) {
    fun getFav(listeFavoris: MutableLiveData<MutableList<TvShow>> = MutableLiveData(mutableListOf()), brToken: String) {
        val queue = Volley.newRequestQueue(application)
        @Suppress("RedundantSamConstructor")
        val r = AuthenticatedStringRequest(
            method = Request.Method.GET,
            url = "https://tvshowapi.apis.redirectme.net/api/favorites",
            bearerToken = brToken,
            {
                val favoris = Gson().fromJson(it, Array<TvShow>::class.java)
                listeFavoris.postValue(favoris.toMutableList())
            },
            {
                Toast.makeText(application, "Login Invalide", Toast.LENGTH_LONG).show()
                println(it.message)
            }
        )
        //Ne pas oublier de placer la requête dans la file pour qu'elle soit exécutée
        queue.add(r)
    }
}