package com.inf5d6.tp1.repositories

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.android.volley.Request
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.gson.Gson
import com.inf5d6.tp1.models.TvShow

class SerieRepository (private val application: Application){
    fun getSeries(series: MutableLiveData<MutableList<TvShow>>){
        val queue = Volley.newRequestQueue(application)
        val r = StringRequest(
            Request.Method.GET,
            "https://tvshowapi.apis.redirectme.net/api/tvshows",
            {
                // Debug
                println("DEBUG: SerieRepository:series:it: $it")
                val arraySeries = Gson().fromJson(it, Array<TvShow>::class.java)
                series.value= arraySeries.toMutableList()
            },
            {
                println("ERREUR: /api/tvshows")
            }
        )// fin StringRequest()
        queue.add(r)
    }// fun getSeries()
}// fin serieRepository()