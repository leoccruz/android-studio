package com.inf5d6.tp1.repositories

import android.app.Application
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.gson.Gson
import com.inf5d6.tp1.auth.AuthenticatedStringRequest
import com.inf5d6.tp1.auth.AuthenticationRequest
import com.inf5d6.tp1.models.DetailsTvShow
import com.inf5d6.tp1.models.IsFavorite
import com.inf5d6.tp1.models.TvShow

class DetailRepository (private val application: Application) {

    private val queue = Volley.newRequestQueue(application)

    fun getDetail(tvShowId: Int, details: MutableLiveData<DetailsTvShow>) {

        val r = StringRequest(
            Request.Method.GET,
            "https://tvshowapi.apis.redirectme.net/api/tvshow?TvShowId=${tvShowId}",
            {
                //Debug
                println("DEBUG: DetailRepository:getDetail:it: $it")
                val infoDetails = Gson().fromJson(it, DetailsTvShow::class.java)
                details.postValue(infoDetails)
            },
            {
                println("ERREUR: /api/artistes")
            })
        queue.add(r)
    }

    fun getFavStatus(tvShowId: Int, brToken: String, estFavoris: MutableLiveData<Boolean>){

        println("DEBUG: getFavStatus:URL: https://tvshowapi.apis.redirectme.net/api/favorites?tvshowid=${tvShowId}")

        @Suppress("RedundantSamConstructor")
        val r = AuthenticatedStringRequest(
            method = Request.Method.GET,
            url = "https://tvshowapi.apis.redirectme.net/api/favorites?tvshowid=${tvShowId}",
            bearerToken = brToken,
            {
                val status = Gson().fromJson(it, IsFavorite::class.java)
                estFavoris.value = status.IsFavorite
                println("DEBUG: estFavoris.value: ${estFavoris.value}")
            },
            {
                Toast.makeText(application, "Login Invalide", Toast.LENGTH_LONG).show()
                println(it.message)
            }
        )
        queue.add(r)
    }

    fun delete(tvShowId : Int, token : String, estFavoris :  MutableLiveData<Boolean>) {

        println("DEBUG: delete:URL: https://tvshowapi.apis.redirectme.net/api/favorites?tvshowid=${tvShowId}")
        println("DEBUG: dans delele:estfavoris: " + estFavoris.value)
        println("DEBUG: dans delete:token: $token")
        println("DEBUG: delete:tvShowId: $tvShowId")
        @Suppress("RedundantSamConstructor")
        val r = AuthenticatedStringRequest(
            Request.Method.DELETE,
            "https://tvshowapi.apis.redirectme.net/api/favorites?tvshowid=${tvShowId}",
            //bearerToken = token + "a",
            bearerToken = token,
            {
                estFavoris.value = false
            },
            {
                println("erreur dans delete")
            }
        )
        queue.add(r)
    }

    fun add(tvShowId : Int, token : String, estFavoris :  MutableLiveData<Boolean>) {

        println("dans delele:estfavoris: " + estFavoris.value)
        println("dans delele:token: " + token)
        println("dans delele:token: " + tvShowId)

        @Suppress("RedundantSamConstructor")
        val r = AuthenticatedStringRequest(
            Request.Method.POST,
            url = "https://tvshowapi.apis.redirectme.net/api/favorites?tvshowid=${tvShowId}",
            bearerToken = token,
            Response.Listener { estFavoris.value = true },
            Response.ErrorListener { println("erreur dans delete") }
        )
        queue.add(r)
    }
}