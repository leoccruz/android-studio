package com.inf5d6.tp1.models

data class Studio(
    val StudioId: Int,
    val Name: String
)