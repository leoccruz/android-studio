package com.inf5d6.tp1.models

@Suppress("unused")
class AuthenticationResponse(
    val access_token: String,
    val emailaddress: String,
    val expires_in: Int,
    val roles: String,
    val token_type: String,
    val userName: String
)