package com.inf5d6.tp1.models

data class Genre(
    val GenreId: Int,
    val Name: String
)