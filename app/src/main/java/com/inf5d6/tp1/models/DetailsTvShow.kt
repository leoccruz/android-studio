package com.inf5d6.tp1.models

@Suppress("unused")
data class DetailsTvShow(
    val EpisodeCount: Int,
    val Genres: List<Genre>,
    val Image: String,
    val Plot: String,
    val Roles: List<Role>,
    val Seasons: List<Season>,
    val Studio: Studio,
    val TVParentalGuideline: String,
    val Title: String,
    val TvShowId: Int,
    val Year: Int
)