package com.inf5d6.tp1.models

@Suppress("unused")
data class TvShow(
    val Genres: List<Genre>,
    val Image: String,
    val SeasonCount: Int,
    val Studio: Studio,
    val Title: String,
    val TvShowId: Int
)