package com.inf5d6.tp1.models

data class Season(
    val SeasonId: Int,
    val EpisodeCount: Int,
    val Image: String,
    val Number: Int,
    val ShortSeasonName: String
)